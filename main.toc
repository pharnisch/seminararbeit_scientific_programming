\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Kurzfassung}{3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Motivation}{7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Neurologischer Hintergrund}{9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Neuronen}{9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Synapsen}{9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Ablauf von Erregungs\IeC {\"u}bertragung}{10}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Neuronale Netze}{11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Neuronale Plastizit\IeC {\"a}t}{11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}Lernvorgang}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.1}Kurzzeitged\IeC {\"a}chtnis}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.2}Langzeitged\IeC {\"a}chtnis}{12}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Grundlagen}{13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}K\IeC {\"u}nstliches Neuron}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}\IeC {\"U}bertragungsfunktion}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}Aktivierungsfunktion}{15}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Fehlerfunktion}{19}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}K\IeC {\"u}nstliche Neuronale Netze}{19}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Lernen eines k\IeC {\"u}nstlichen neuronalen Netzes}{21}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Vorstellung spezifischer Modelltypen}{23}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Autoencoder}{24}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Definitionen}{25}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Netzdurchlauf}{26}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}Lernen mit Backpropagation}{27}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.4}Beispiel}{29}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.5}Bewertung}{29}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Rekurrente neuronale Netze}{30}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Definitionen}{31}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Netzdurchlauf}{32}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Lernen mit Backpropagation}{33}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.4}Beispiel}{36}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.5}Bewertung}{37}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Selbstorganisierende Karten}{38}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Definitionen}{39}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}Ablauf}{40}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.3}Beispiel}{41}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.4}Bewertung}{42}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Allgemeine Bewertung}{43}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Vergleich von k\IeC {\"u}nstlichen neuronalen Netzen mit dem menschlichen Gehirn}{43}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Neurologische Plausibilit\IeC {\"a}t}{43}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Ungeeignete Probleme}{44}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Ausblick}{45}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Ethische Aspekte}{45}
