\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {0}{1}{Neuronen}{5}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {0}{2}{Synapsen}{6}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {0}{3}{Ablauf von Erregungs\IeC {\"u}bertragung}{7}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {0}{4}{Neuronale Netze}{9}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {0}{5}{Lernvorgang}{10}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {0}{6}{K\IeC {\"u}nstliches Neuron}{11}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {0}{6}{1}{\IeC {\"U}bertragungsfunktion}{14}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {0}{6}{2}{Aktivierungsfunktion}{15}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {0}{7}{Fehlerfunktion}{19}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {0}{8}{K\IeC {\"u}nstliche Neuronale Netze}{21}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {0}{9}{Lernen eines k\IeC {\"u}nstlichen neuronalen Netzes}{24}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {0}{10}{Autoencoder}{28}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {0}{10}{1}{Netzdurchlauf}{31}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {0}{10}{2}{Lernen mit Backpropagation}{35}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {0}{10}{3}{Beispiel}{40}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {0}{10}{4}{Bewertung}{41}{0}{0}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {0}{11}{Ungeeignete Probleme}{42}{0}{0}
