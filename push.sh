#!/bin/bash
# commit message is given as first param
COMMIT_MESSAGE=$1
if ["$COMMIT_MESSAGE" = ""] # is empty
then
	COMMIT_MESSAGE="Keine Commit-Nachricht angegeben"
fi

ADD="git add ."
echo ============= $ADD
echo -ne '(33%) #### \r'
$ADD

COMMIT='git commit -m "$COMMIT_MESSAGE"'
echo ============= $COMMIT
echo -ne '(66%) ######## \r'
$COMMIT

PUSH="git push"
echo ============= $PUSH
echo -n '(100%) ############ \r'
$PUSH

echo Erfolgreich versioniert.
