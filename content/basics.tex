\chapter{Grundlagen}
\label{chap:grundlagen}

	Dieses Kapitel befasst sich mit den Grundlagen von künstlichen neuronalen Netzen. Es wird ebenfalls Bezug zum neurologischen Vorbild -- dem menschlichen Gehirn -- hergestellt.

\section{Künstliches Neuron}
\label{sec:kuenstliches_neuron}

	Ein künstliches Neuron ist die Grundlage von künstlichen neuronalen Netzen. Dabei wird das komplexe biologische Vorbild in seiner Struktur sehr stark vereinfacht dargestellt. Es entspricht in einem Netz einem Knoten, von denen im Netz viele in oftmals hintereinanderliegenden Schichten angeordnet werden. Ein Neuron innerhalb solch einer Schicht wird häufig wie folgt modelliert (zu sehen in \cref{fig:kuenstliches_neuron}):
	
	\begin{figure}[H]
		\includegraphics[width=1\linewidth]{images/kuenstliches_neuron.png}
		\caption[Künstliches Neuron]{Künstliches Neuron mit Beschriftung seiner Bestandteile, Quelle:\,de.wikipedia.org (Indizes wurden nachträglich vereinfacht)}
		\label{fig:kuenstliches_neuron}
	\end{figure}

	Ein Eingabevektor $\vec{x}$ (Dieser repräsentiert die Endknöpfchen bei menschlichen Neuronen respektive dessen Abgabe von Botenstoffen in den synaptischen Spalt.) trifft auf einen Gewichtungsvektor $\vec{w}$ (Dieser entspricht den unterschiedlich stark ausgeprägten Synapsen beim Menschen, also repräsentiert die Aufnahme von Reizen, zum Beispiel in Form von Botenstoffen im synaptischen Spalt, durch Rezeptoren.). Der Gewichtungsvektor kann sowohl zu hemmenden, also inhibitorischen, ($w_{k} < 0$) als auch erregenden, also exzitatorischen, ($w_{k} > 0$) Eingaben für die darauffolgende Aktionsbildung führen. Dabei steht der Index $k$ für die Position der jeweiligen Gewichtung im Gewichtungsvektor, der zumeist mit zufälligen reellen Werten aus einem bestimmten Bereich initialisiert wird. Ein Gewicht von $0$ entspricht einer nicht existenten Verbindung zwischen zwei Neuronen-Knoten. Es gilt:
	\[ \vec{x}, \vec{w} \in \mathbb{R}^{n}, n \in \mathbb{N} \text{ .} \]
	
	Anschließend wird der vorherige Ergebnisvektor von der Übertragungsfunktion $\Sigma$ (näher erläutert in \cref{subsec:uebertragungsfunktion}) auf die reellen Zahlen abgebildet. ($\Sigma$ kann als Nachahmung der räumlichen Summation elektronischer Impulse bei echten Neuronen interpretiert werden.) Als Eingabe dazu dienen der Eingabevektor $\vec{x}$ und der Gewichtungsvektor $\vec{w}$:
	\[ \Sigma: (\mathbb{R}^{n}, \mathbb{R}^{n}) \rightarrow \mathbb{R} \text{ .} \]
	Das Ergebnis von $\Sigma$  wird als $net$ bezeichnet: (Dies entspricht dem Aktionspotential am Ort des Axonhügels, welches sich vorher im Zellkörper ausgebreitet hat.)
	\[ \Sigma(\vec{x}, \vec{w}) = net \text{ .} \]
	
	Zusätzlich wird das Aktionspotential durch den Schwellenwert $\Theta \in \mathbb{R}$ (Dieser entspricht im Gehirn dem elektrischen Potential, welches erreicht werden muss, um den Reiz in Form elektronischer Spannung weiter zu leiten.) vermindert. Dann wird das Ergebnis der Subtraktion mithilfe der Aktivierungsfunktion (siehe \cref{subsec:aktivierungsfunktion}) auf den Ausgabewert $o$ abgebildet (Die Aktivierungsfunktion entspricht bei einem natürlichen Neuron der Übermittlung der elektronischen Signale über das Axon hinweg.):
	\[ o = \varphi(net - \Theta) \text{ .} \] 
	
	Der Schwellenwert (ferner Bias genannt) kann in der Praxis vereinfacht umgesetzt werden, indem er in Form eines sogenannten on-Neurons, als weiterer Eingabewert $x_0 = 1$ mit Gewichtung $w_{0}$ modelliert wird. Dabei wird das Gewicht folgendermaßen initialisiert:
	\[ w_{0} = -\Theta \text{ .} \]
	
	Mit diesem Schritt kann die gesonderte Behandlung des Schwellenwertes entfallen und $net$ ergibt das bereits um den Schwellenwert $\Theta$ geminderte Aktionspotential, mit dem fortan die Ausgabe direkt berechnet werden kann:
	\[ o = \varphi(net) \text{ .} \]

	Dabei werden die Übertragungs- und Aktivierungsfunktion insgesamt als Transferfunktionen bezeichnet. In dieser Arbeit wird allgemein davon ausgegangen, dass die Übertragungsfunktion den Schwellenwert als on-Neuron verrechnet.
	\Cite{wiki-kuenstliches_neuronales_netz} \Cite{transferfunktionen}
	
\subsection{Übertragungsfunktion}
\label{subsec:uebertragungsfunktion}

	Die Übertragungsfunktion $\Sigma$ ist eine lineare Abbildung. Sie ist dafür zuständig, die Eingaben $\vec{x}$ und die Gewichtungen $\vec{w}$ auf einen reellen Wert abzubilden:
	\[ \Sigma: (\mathbb{R}^{n}, \mathbb{R}^{n}) \rightarrow \mathbb{R} \text{ .} \]
	
	Dabei ist im Allgemeinen nicht vorgeschrieben, wie diese Abbildung auszusehen hat. Jedoch ist es üblich, die triviale Skalarprodukt-Übertragung zu wählen:
	\begin{equation} \label{skalar_produkt_uebertragung}
	net = \Sigma(\vec{x}, \vec{w}) = \langle\vec{x}, \vec{w}\rangle = \vec{x}^{\top} \cdot \vec{w} \text{ .}
	\end{equation}
	
	Als Alternative zur Skalarprodukt-Übertragung existieren weitere Übertragungsfunktionen auf der Grundlage von verschiedenen Distanzmaßen. Die Übertragung eines Neurons erfolgt dabei bezüglich der Distanz seines Gewichtungsvektors $\vec{w}$ zum Eingabevektor $\vec{x}$. Es können verschiedene Maße verwendet werden, wovon eine Auswahl im Folgenden aufgeführt wird:
	\begin{description}
		\item[Euklidische Distanz:] $ net = \Sigma(\vec{x}, \vec{w}) = \lVert \vec{x} - \vec{w} \rVert_2 = \sqrt{\sum_{k=1}^{n}{(x_k - w_{k})^2}}$
		\item[Manhattan-Distanz:] $ net = \Sigma(\vec{x}, \vec{w}) = \lVert \vec{x} - \vec{w} \rVert_1 = \sum_{k=1}^{n}{\lvert x_k - w_{k} \rvert}$
	\end{description}
	\Cite{transferfunktionen}
	
\subsection{Aktivierungsfunktion}
\label{subsec:aktivierungsfunktion}

	Die Aktivierungsfunktion $\varphi$ wird verwendet zum Abbilden des um den Schwellenwert $\Theta$ verminderten Aktionspotentials $net$:
	\[ \varphi: \mathbb{R} \rightarrow O \text{ ,} \]
	wobei $O \subseteq \mathbb{R}$ die Menge der möglichen Ausgabewerte ist. Im Folgenden sei im Allgemeinen ${O}\,=\,[0,\,1]$, jedoch sind theoretisch auch andere Mengen denkbar. 
	
	Dabei gibt es verschiedene Konstruktionen dieser Funktion in der Praxis, wobei die Verwendung davon abhängt, welche Netztopologie verwendet wird. Als übliche Charaktereigenschaft einer Aktivierungsfunktion kann ihre monotone Steigung genannt werden. Eine weitere Eigenschaft stellt eine begrenzte Zielmenge ${O}$ dar. (Der obere Grenzwert kann hier als Refraktärzeit des Neuronen verstanden werden.) Nachfolgend werden grundlegende Aktivierungsfunktionen vorgestellt.
	
	Der Einsatz linearer Aktivierungsfunktionen wie in \cref{fig:lineare_funktion} ist nur bedingt sinnvoll (nur in sehr einfachen Modellen), da eine Komposition mehrerer Aktivierungsfunktionen dieses Typs nach arithmetischen Umformungen durch eine einzige lineare Funktion dargestellt werden kann.
	
	\definecolor{col1}{rgb}{0.3921,0.7031,0.3320} 
	\definecolor{col2}{rgb}{0.1563,0.6641,0.5859}  
	\definecolor{col3}{rgb}{0.0000,0.5859,0.8203} 
	\definecolor{col4}{rgb}{0.0000,0.4102,0.7031} 
	
	\begin{figure}[H]
		\begin{center}
			\begin{tikzpicture}
			\begin{axis}[
			axis x line=center,
			axis y line=center,
			xtick={-1,0,1},
			ytick={-1,0,1},
			xlabel={$net$},
			ylabel={$o$},
			xlabel style={right},
			ylabel style={above},
			ymin=-2,
			ymax=2,
			xmin=-2,
			xmax=2,
			grid=major,
			style={ultra thick},
			]
			\addplot [
			col1,
			domain=-2:2,
			]
			{(1/2) * x + 0.5};
			\end{axis}
			\end{tikzpicture}
			\caption[Lineare Funktion]{Eine lineare Funktion $\varphi(net) = 0.5 + 0.5 \cdot net$}
			\label{fig:lineare_funktion}
		\end{center}
	\end{figure}
	
	In \cref{fig:stueckweise_lineare_funktion} ist eine Aktivierungsfunktion zu sehen, welche abschnittsweise definiert ist. Die einzelnen Abschnitte sind jeweils linear.
	
	\begin{figure}[H]
		\begin{center}
			\begin{tikzpicture}
			\begin{axis}[
			axis x line=center,
			axis y line=center,
			xtick={-1,0,1},
			ytick={-1,0,1},
			xlabel={$net$},
			ylabel={$o$},
			xlabel style={right},
			ylabel style={above},
			ymin=-2,
			ymax=2,
			xmin=-2,
			xmax=2,
			grid=major,
			style={ultra thick},
			]
			\addplot [
			col2,
			domain=-2:-0.5,
			]
			{0};
			\addplot [
			col2,
			domain=-0.5:0.5,
			]
			{x + 0.5};
			\addplot [
			col2,
			domain=0.5:2,
			]
			{1};
			\end{axis}
			\end{tikzpicture}
			\caption[Stückweise lineare Funktion]{Eine stückweise lineare Funktion $\varphi(net) = \begin{cases}
				net \leqslant -0.5 & 0\\
				-0.5 < net \leqslant 0.5 & net + 0.5\\
				net > 0.5 & 1\\
				\end{cases}$}
			\label{fig:stueckweise_lineare_funktion}
		\end{center}
	\end{figure}
	
	In \cref{fig:schwellenwert_funktion} ist eine Aktivierungsfunktion zu sehen, welche von einem zum anderen Ausgabewert springt, abhängig davon, ob mit der Eingabe eine Schwelle erreicht beziehungsweise übertroffen wurde. Hierbei bildet die Schwellenwertfunktion speziell auf ${O} = \{0, 1\}$ ab.
	
	\begin{figure}[H]
		\begin{center}
			\begin{tikzpicture}
			\begin{axis}[
			axis x line=center,
			axis y line=center,
			xtick={-1,0,1},
			ytick={-1,0,1},
			xlabel={$net$},
			ylabel={$o$},
			xlabel style={right},
			ylabel style={above},
			ymin=-2,
			ymax=2,
			xmin=-2,
			xmax=2,
			grid=major,
			style={ultra thick},
			]
			\addplot [
			col3,
			domain=-2:0,
			]
			{0};
			\addplot [
			col3,
			domain=0:2,
			]
			{1};
			\end{axis}
			\end{tikzpicture}
			\caption[Schwellenwertfunktion]{Eine Schwellenwertfunktion $\varphi(net) = \begin{cases}
				net \leqslant 0 & 0\\
				net > 0 & 1\\
				\end{cases}$}
			\label{fig:schwellenwert_funktion}
		\end{center}
	\end{figure}
	
	In \cref{fig:sigmoide_funktion} sind verschiedene Ausprägungen von sigmoiden Funktionen zu sehen. Diese sind häufig in der Praxis anzutreffen. Abhängig von ihrem freien Parameter $\beta$ nehmen diese sehr unterschiedliche Formen an. Durch ihre streng monotone Steigung, Stetigkeit und Differenzierbarkeit sind sie von besonderer Bedeutung für die sogenannten Backpropagation-Netze\footnote{\textbf{Backpropagation} ist ein allgemeines Lernverfahren für überwachte künstliche neuronale Netze, bei dem die Fehler rückwärts durch die Schichten propagiert werden und dabei Anpassungen der Gewichte stattfinden. Auf dieses wird im Rahmen von \cref{auto_encoder} sowie \cref{rnn} näher eingegangen.}, welche diese voraussetzen. In dieser Arbeit sei $\varphi$ im Allgemeinen eine sigmoide Aktivierungsfunktion mit $\beta = 1$:
	\begin{equation} \label{sigmoide_aktivierungs_funktion}
	o = \varphi(net) = \frac{1}{1 + e^{-net}} \text{ .}
	\end{equation}
	\Cite{wiki-kuenstliches_neuron} \Cite{transferfunktionen}
	
	\begin{figure}[H]
		\begin{center}
			\begin{tikzpicture}
			\begin{axis}[
			axis x line=center,
			axis y line=center,
			xtick={-1,0,1},
			ytick={-1,0,1},
			xlabel={$net$},
			ylabel={$o$},
			xlabel style={right},
			ylabel style={above},
			ymin=-2,
			ymax=2,
			xmin=-2,
			xmax=2,
			grid=major,
			style={ultra thick},
			legend entries={$\beta = 1$, $\beta = 5$, $\beta = 10$},
			]
			\addplot [
			col4,
			domain=-2:2,
			]
			{1 / (1 + exp(- (x * 1)))};
			\addplot [
			col1,
			domain=-2:2,
			samples=201,
			]
			{1 / (1 + exp(- (x * 5)))};
			\addplot [
			col2,
			domain=-2:2,
			samples=201,
			]
			{1 / (1 + exp(- (x * 10)))};
			\end{axis}
			\end{tikzpicture}
			\caption[Sigmoide Funktion]{Sigmoide Funktionen $\varphi_\beta(net) = \frac{1}{1 + e^{- \beta \cdot net}}$}
			\label{fig:sigmoide_funktion}
		\end{center}
	\end{figure}

	In \cref{fig:tangens_hyperbolicus} ist ebenfalls eine differenzierbare Aktivierungsfunktion zu sehen. Es gilt zu beachten, dass $O = (-1, 1)$.
	
	\begin{figure}[H]
		\begin{center}
			\begin{tikzpicture}
			\begin{axis}[
			axis x line=center,
			axis y line=center,
			xtick={-1,0,1},
			ytick={-1,0,1},
			xlabel={$net$},
			ylabel={$o$},
			xlabel style={right},
			ylabel style={above},
			ymin=-2,
			ymax=2,
			xmin=-2,
			xmax=2,
			grid=major,
			style={ultra thick},
			]
			\addplot [
			col3,
			domain=-2:2,
			]
			{tanh(x)};
			\end{axis}
			\end{tikzpicture}
			\caption[Tangens hyperbolicus]{Tangens hyperbolicus $\varphi(net) = \tanh(net)$}
			\label{fig:tangens_hyperbolicus}
		\end{center}
	\end{figure}	

	Eine weitere differenzierbare Aktivierungsfunktion, welche auch alle anderen Neuronen der Schicht mit in die Rechnung einfließen lässt, ist die sogenannte Softmax-Funktion. Sie wandelt den $\vec{net}$-Vektor einer Schicht um in einen Ausgabevektor $\vec{o}$, dessen Elemente innerhalb des Intervalls $[0, 1]$ liegen und aufsummiert $1$ ergeben. Im Kontext der Softmax-Aktivierungsfunktion ist diese speziell für den gesamten Vektor definiert:
	\[ \varphi(\vec{net}) : \mathbb{R}^n \rightarrow \{ \vec{o} \in \mathbb{R}^n \mid o_j \ge 0, \sum_{j=1}^{n}{o_j} = 1 \} \text{ .} \]
	Die einzelnen $\vec{net}$-Vektorelemente dafür lassen sich wie folgt berechnen:
	\[ o_j = \frac{e^{net_j}}{\sum_{i=1}^{n}{e^{net_i}}}, \text{für} j = 1,\,\dots, n \text{ .} \]
	\Cite{softmax}

\section{Fehlerfunktion}
\label{sec:fehler_funktion}

	Die Fehlerfunktion $f$ ist essentiell beim sogenannten überwachten Lernen, denn sie ist die Metrik, die entscheidet, in welchem Ausmaß Abweichungen der Ausgaben $o$ von der gewünschten Ausgabe $y$ als Fehler interpretiert werden:
	\[ f : \mathbb{R}^{n} \rightarrow \mathbb{R}, n \in \mathbb{N} \text{ .} \]
	
	Triviale Metriken dazu, welche auch Einsatz in der Praxis finden, sind die als L1-Fehler (englisch L1 loss) und L2-Fehler (englisch L2 loss) bezeichneten Fehlerfunktionen. Der L1-Fehler (siehe \cref{L1_loss}) beobachtet die Summe der absoluten Fehler, der L2-Fehler (siehe \cref{L2_loss}) hingegen betrachtet die Summe der quadrierten Fehler:
	\begin{equation}\label{L1_loss}
	f_\text{L1}(y, o) = \sum_{i=1}^{n} | y_i - o_i | \text{ ,}
	\end{equation}
	\begin{equation}\label{L2_loss}
	f_\text{L2}(y, o) = \sum_{i=1}^{n} ( y_i - o_i )^2 \text{ .}
	\end{equation}
	
	Als eine weitere Metrik zur Bestimmung der Fehlergröße sei hier noch die Kreuzentropie\footnote{\textbf{Kreuzentropie} ist ein Qualitätsmaß eines Modells für eine Wahrscheinlichkeitsverteilung.} erwähnt. Der Kreuzentropie-Fehler (siehe \cref{crossentropy_loss}) ist geeignet zum Messen von Fehlern bei Vektoren, dessen Elemente Wahrscheinlichkeiten darstellen:
	\begin{equation}\label{crossentropy_loss}
	f_\text{Kreuzentropie}(y, o) = - \sum_{i=1}^{n} y_i \ln ( o_i ) \text{ .}
	\end{equation}
	\Cite{error_functions}

\section{Künstliche Neuronale Netze}

	Künstliche neuronale Netze sind ein Verbund künstlicher Neuronen. Dabei werden diese zumeist in Schichten angeordnet. Es werden die Neuronen der vordersten Schicht als Eingabe-Neuronen bezeichnet. Die Eingabe-Neuronen weichen jedoch von den anderen Neuronen-Schichten ab, indem sie nur dafür zuständig sind, jeweils einen Eingabewert aufzunehmen und an das nächste Neuron oder die nächsten Neuronen der nächsten Schicht weiterzugeben. Die Eingabe-Schicht ist somit nur dafür zuständig, die Eingabedaten in das Netz zu transportieren. Daher wird sie zum Bezeichnen eines Netzes nicht mitgezählt. Die Neuronen der letzten Schicht werden als Ausgabe-Neuronen bezeichnet. Die Schichten dazwischen bestehen aus sogenannten versteckten Neuronen (auch Zwischen-Neuronen genannt). Die versteckten Neuronen sowie die Ausgabe-Neuronen funktionieren dabei so wie in \cref{sec:kuenstliches_neuron} beschrieben.
	
	Die Anordnung von Neuronen ist nicht trivial. Es gibt viele verschiedene topologische Modelle, die unterschiedliche Ziele erreichen sollen, wovon einige im Rahmen von \cref{chap:different_types} näher vorgestellt werden.
	
	Grundlegend wird bei künstlichen neuronalen Netzen danach kategorisiert, wie viele Schichten ein Netz besitzt. Ein simples einschichtiges Netz stellt das Perzeptron\footnote{Das \textbf{Perzeptron} ist ein einfaches künstliches neuronales Netz nach Frank Rosenblatt aus dem Jahre 1958.} (siehe \cref{fig:perzeptron}) in seiner Grundversion (dann auch als einlagiges Perzeptron bezeichnet) dar:
	
	\begin{figure}[H]
		\begin{center}
			\includegraphics[width=0.5\linewidth]{images/perzeptron.png}
			\caption[Einlagiges Perzeptron]{Ein einlagiges Perzeptron, Quelle: commons.wikimedia.org}
			\label{fig:perzeptron}
		\end{center}
	\end{figure}
	
	$x_0$ ist hier der Bias. Die anderen $x_i$ sind die Eingaben. Es gibt also nur ein verarbeitendes Neuron. Beim Perzeptron wird als Übertragungsfunktion die Skalarprodukt-Übertragungsfunktion genutzt. Durch diese ergibt sich mathematisch wie folgt der Ausgabewert:
	\[ o = \varphi(
	{\begin{pmatrix}
		w_0 &
		w_1 &
		\ldots &
		w_n\\
		\end{pmatrix}}%^{\top}
	\cdot
	\begin{pmatrix}
	1\\
	x_1\\
	\ldots\\
	x_n\\
	\end{pmatrix}
	)
	\text{ .}
	\]
	
	Es gibt auch die weitergedachte Netz-Architektur-Variante, in denen mehrere Perzeptren (mindestens eine zusätzliche Hidden-Schicht zum einlagigen Perzeptron) als Verbund fungieren. In diesem Fall wird von einem mehrlagigen Perzeptron gesprochen. Ein mehrlagiges Perzeptron, bei dem alle Neuronen einer Schicht genau mit jedem Neuron der nächsten Schicht verbunden sind, wird auch als vorwärtsgerichtetes Netz (auch Feedforward-Netz) bezeichnet.
	
	\begin{figure}[H]
		\begin{center}
			\includegraphics[width=0.5\linewidth]{images/mehrlagiges_perzeptron.png}
			\caption[Mehrlagiges Perzeptron]{Ein mehrlagiges Perzeptron, Quelle: de.wikipedia.org}
			\label{fig:mehrlagiges_perzeptron}
		\end{center}
	\end{figure}
	\Cite{wiki-perzeptron} \Cite{perceptron}

\section{Lernen eines künstlichen neuronalen Netzes}

	Künstliche neuronale Netze können theoretisch durch Anpassung verschiedenster freier Parameter ein Lernverhalten simulieren:
	\begin{itemize}
		\item Erzeugen von Neuronen
		\item Löschen von Neuronen
		\item Erzeugen von Verbindungen
		\item Löschen von Verbindungen
		\item Anpassung von Verbindungsausprägungen (Gewichte)
		\item Anpassung des Schwellenwertes (beispielsweise in Form von Anpassung der Bias-Gewichte)
		\item Anpassung der Übertragungsfunktion
		\item Anpassung der Aktivierungsfunktion
	\end{itemize}
	
	Auch wird in der Praxis ein Lernparameter $\eta$ gewählt, welcher die Lernrate beschreibt. Er bestimmt, wie stark die anderen Parameter pro Lernschritt angepasst werden und wird zumeist mit steigender Anzahl von Lernschritten vermindert, um starke Schwankungen der Parameter im Lernprozess zu reduzieren. Damit soll sichergestellt werden, dass der Lernprozess konvergiert.
	\Cite{wiki-kuenstliches_neuronales_netz}
	
	Gewöhnlich kann zwischen zwei verschiedenen Phasen beim Lernen eines Netzes unterschieden werden. Dazu zählt eine Trainingsphase, in der das Netz mithilfe von Lehrmaterial lernt (zumeist Gewichtungen verändert), und eine Testphase, in der der Lernerfolg auf die Probe gestellt wird. In der Testphase werden keine Parameter mehr angepasst, es soll also nicht weiter gelernt, sondern lediglich geprüft werden, ob das Lernen erfolgreich war. Dafür wird das Netz entweder erneut mit dem vorherigen Trainingsmaterial oder mit neuen Eingangsreizen $\vec{x}_{\text{new}}$ konfrontiert. Wenn das Netz es schafft, die neuen Reize zufriedenstellend zu verarbeiten, so fand erfolgreich eine Generalisierung des Problems statt. Kann das Netz jedoch nur die Lehrreize erfolgreich verarbeiten und nicht die neuen, so fand eine Übergeneralisierung\footnote{Eine \textbf{Übergeneralisierung} (englisch \textbf{Overfitting}) entspricht dem Auswendiglernen des Lehrmaterials. Der Begriff dient zum Ausdruck einer gescheiterten Generalisierung.} statt. (Hiergegen hilft unter anderem die Modifikation des Lernparameters $\eta$.)
	\Cite{neuronalesnetz_de}