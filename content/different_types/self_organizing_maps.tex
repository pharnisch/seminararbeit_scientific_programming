\section{Selbstorganisierende Karten}
\label{sec:selbstorganisierende_karten}

	Selbstorganisierende Karten (auch Kohonennetze gennant) sind spezielle vorwärtsgerichtete künstliche neuronale Netze, die unüberwacht lernen und deren Zweck es ist, die Dimension von Eingabedaten zu reduzieren (ähnlich dem Ziel von Autoencodern, siehe \cref{auto_encoder}). Somit ist ein typisches Einsatzgebiet die Visualisierung von mehrdimensionalen Daten auf niedrig-dimensionale Karten, nachdem die Daten auf die Dimension der Karte reduziert wurden.
	
	%Als neurologische Motivation dient die Erkenntnis, dass viele Strukturen im Gehirn eine lineare Topologie aufweisen, die Signale des Eingangsraums (beispielsweise visuelle Reize) jedoch multidimensional sind.
	Hierbei haben die selbstorganisierenden Karten topologische Merkmalskarten\footnote{\textbf{Merkmalskarten} werden als Dimensionsreduktion von Eingangsreizen im menschlichen Gehirn verstanden und werden von der Merkmalsintegrationstheorie formuliert.} im Gehirn als Vorbild, die ähnliche (mehr-dimensionale) Reize nah beieinander kartieren. 
	
	Die selbstorganisierenden Karten bestehen nur aus einer Eingabe- und einer Ausgabe-Schicht. Es gibt keine versteckten Schichten und die Ausgabeschicht (mit ihren Gewichten) übernimmt die Aufgabe der Zuweisung von Datenpunkten auf die Karte. Jene modellierten Karten können im Allgemeinen beliebig strukturiert sein. In der Praxis stellen die einzelnen Neuronen der Ausgabeschicht jedoch häufig einen Knotenpunkt eines Quadrat-Gitters (siehe \cref{img:som}) oder einer hexagonalen Topologie dar.
	
	In \cref{som_defs} werden wichtige mathematische Definitionen gemacht, welche erforderlich sind, um den Ablauf in \cref{som_ablauf} vollständig nachvollziehen zu können.
	\Cite{wiki-selbstorganisierende_karte}
	
	\begin{figure}[htb]
		\begin{center}
			\includegraphics[width=0.7\linewidth]{images/som.png}
			\caption[Aufbau selbstorganisierender Karte]{Aufbau selbstorganisierender Quadrat-Gitter-Karte mit $X \subseteq \mathbb{R}^3$,\\ Quelle: upload.wikimedia.org}
			\label{img:som}
		\end{center}
	\end{figure}
	
	\subsection{Definitionen}
	\label{som_defs}
	
		$X \subseteq \mathbb{R}^{n}$ ist der Raum der Eingabereize. Die Eingabedaten sind also Elemente aus $X$.
		
		$K \subseteq \mathbb{R}^l$ mit $l \in \mathbb{N}$ sei hier der Raum der Quadrat-Gitter-Karte, welche im Allgemeinen $l$ Dimensionen haben kann (Diese Topologie ist nicht zwingend, theoretisch kann der Raum der Karte auch anders strukturiert werden.). Es bietet sich für eine anschauliche Visualisierung eine Dimension $l \leq 3$ an.
		
		Die Trainingsreize sind gegeben als eine $\mu_M$-elementige Menge $M$:
		\[ M = \{ x_i \mid x_i \in X \subseteq \mathbb{R}^{n}, i = 1, \ldots, \mu_{M}, \mu_{M} \in \mathbb{N} \} \text{ .}\]
		
		Weiterhin gibt es die $\mu_N$-elementige Menge $N$, in der die Ausgabe-Neuronen des Netzes enthalten sind, die neben ihrem Gewichtungsvektor $w_i$ auch aus ihrer Position $k_i$ auf der $l$-dimensionalen Karte bestehen:%(im Normalfall $l=2$) 
		\[ N = \{ n_i = (w_i, k_i) \mid w_i \in X \subseteq \mathbb{R}^{n}, k_i \in K \subseteq \mathbb{R}^{l}, i = 1, \ldots, \mu_N, \mu_{N} \in \mathbb{N} \} \text{ .}\]
		
		Auf dem Eingaberaum $X$ ist die Metrik $d_X$ zum Bestimmen eines Gewinnerneurons\footnote{Das \textbf{Gewinnerneuron} (englisch \textbf{Best Matching Unit}) ist das Neuron, dessen Gewichtsvektor die geringste Distanz (abhängig von der Metrik) zum Eingabereiz hat. } ${n_s} = (w_s, k_s)$ definiert.
		Für gewöhnlich wird für die Metrik $d_X$ die euklidische Distanz verwendet:
		\[ d_X({x}_j, {w}_i) = \lVert {x}_j - {w}_i \rVert, x_j, w_i \in X \text{ .}\]
		
		Auf der Karte $K$ ist die Metrik $d_K$ zum Bestimmen des Abstandes zwischen Gewinnerneuron und anderen Neuronen definiert, welche in der Praxis ebenfalls häufig die euklidische Norm ist:
		\[ d_K({k}_s, {k}_i) = \lVert {k}_s - {k}_i \rVert, k_s, k_i \in K \text{ .}\]
		
		$t \in \{ 0, \ldots, t_{\text{max}} \}$ ist der Iterationsschritt in der Lernphase. $t_{\text{max}}$ ist die letzte Iteration.
		
		$\eta \in \left(0, 1\right]$ ist die Lernrate. Sie beeinflusst global das Maß der Gewichtsmodifikationen. Sei ${\eta}_{\text{start}}$ die Lernrate zu Beginn und ${\eta}_{\text{end}}$ die Lernrate in der letzten Iteration. So kann die von $t$ abhängige Lernrate wie folgt definiert werden:
		\[ \eta^t = \eta_{\text{start}} \cdot {\left( \frac{\eta_\text{end}}{\eta_\text{start}} \right)}^{\frac{t}{t_\text{max}}} \text{ .}\]
		
		$\sigma \in \mathbb{R}$ ist der Adaptionsradius, der bestimmt, in welcher Entfernung Neuronen vom Gewinnerneuron bei der Modifikation noch beeinflusst werden können. Sei ${\sigma}_{\text{start}}$ der Adaptionsradius zu Beginn und ${\sigma}_{\text{end}}$ der Adaptionsradius in der letzten Iteration. Dann kann der von $t$ abhängige Adaptionsradius wie folgt definiert werden:
		\[ \sigma^t = \sigma_{\text{start}} \cdot {\left( \frac{\sigma_\text{end}}{\sigma_\text{start}} \right)}^{\frac{t}{t_\text{max}}} \text{ .}\]
		Es werden die Neuronen $n_i=(w_i, k_i)$ als nah genug (auch Nachbar-Neuronen genannt) zum Gewinnerneuron $n_s=(w_s, k_s)$ definiert, für die gilt:
		\[ d_K({k}_s, {k}_i) \leq \sigma \text{ .}\]
		Diese Neuronen werden in der Menge $N^+ \subseteq N$ zusammengefasst:
		\[ N^+ = \{ n_i = (w_i, k_i) \mid d_K({k}_s, {k}_i) \leq \sigma \} \text{ .}\]
		
		Die von $t$ abhängige Entfernungsgewichtungsfunktion $h_{si}$ beeinflusst zusätzlich zu der Lernrate $\eta$ das Maß der Gewichtsmodifikation in Relation zu der Distanz zwischen Gewinnerneuron $n_s$ und einem weiteren Neuron $n_i$. Sie kann wie folgt festgelegt werden:
		\[ h_{si}^t = e^{\frac{-{d_K({k}_s, {k}_i)}^2}{2\cdot(\sigma^t)^2}} \text{ .}\]
		\Cite{wiki-selbstorganisierende_karte}
		
	\subsection{Ablauf}
	\label{som_ablauf}
	
		Die Gewichtsvektoren $w_i$ und die Kartenpositionen $k_i$ werden den Neuronen $n_i$ der Ausgabeschicht zugeordnet. Die Gewichte werden dabei zufällig gewählt und es gilt:
		\[ w_i \in X, k_i \in K \text{ .}\]
		
		Die Lehrreize aus $M$ werden in zufälliger Reihenfolge ausgewählt und in das Netz eingespeist. Ein Durchlauf aller Lehrreize aus $M$ wird als Epoche bezeichnet und ein Training kann aus mehreren Epochen bestehen. Pro Iteration $t$ wird ein Eingabereiz $x_j$ ausgewählt und ein Neuron als Gewinnerneuron $n^t_s$ entschieden, für welches gilt:
		\[ d_X(x^t_j, w^t_s) = min\{ d_X(x^t_j, w^t_i) \mid i=1, \ldots, \mu_N \} \text{ .} \]
		
		Alle Neuronen mit ausreichend geringem Abstand ($d_K(k_s^t, k_i^t) \leq \sigma^t$) zum Gewinnerneuron werden folgendermaßen modifiziert: 
		\[ w^{t+1}_i = w^t_i + \eta^t \cdot h^t_{si} \cdot (x_j - w^t_i) \text{ .}\]
		
		Formal lässt sich dies zusammenfassen als:
		\begin{equation} \label{som_weight_update}
		w^{t+1}_i = \begin{cases}
		w^t_i + \eta^t \cdot h^t_{si} \cdot (x_j - w^t_i) & d_K({k_s}^t, {k_i}^t) \leq \sigma^t\\
		w^t_i & d_K({k_s}^t, {k_i}^t) > \sigma^t\\
		\end{cases} \text{ .}
		\end{equation}
		
		Die Modifikation der Gewichtsvektoren entspricht einer Verschiebung der Gewichte $w$ hin zum Lehrreiz $x$, was in \cref{img:som_training} visualisiert wurde. Dort ist erkennbar, wie zu einem Datenpunkt $x$ (weißer Kreis in der blau dargestellten Menge der gesamten Daten) das Gewinnerneuron $w_s$ (mit kräftigem gelb hervorgehoben) so modifiziert wird, dass die Distanz zwischen Gewinnerneuron und Datenpunkt reduziert wird. Eine Beeinflussung von benachbarten Neuronen (Nachbarschaftsradius ist mit leichtem gelb gekennzeichnet) ist auch in der Darstellung zu sehen. Die Modifikation der Nachbarneuronen ist stärker je näher diese am Gewinnerneuron sind und entspricht der Erregung der Nachbarneuronen durch die laterale Umfeldhemmung\footnote{Die \textbf{laterale Umfeldhemmung} besagt, dass Gruppen von Neuronen andere Gruppen in ihrer Umgebung hemmen. Sie beschreibt eine erregende Wirkung für kurze Distanzen und eine schwächende Wirkung für lange Distanzen.} im Gehirn, wobei im Fall einer selbstorganisierenden Karte keine direkte Hemmung der vom Gewinnerneuron zu weit entfernten Neuronen stattfindet.
		\Cite{wiki-selbstorganisierende_karte}
		
		\begin{figure}[H]
			\begin{center}
				\includegraphics[width=1\linewidth]{images/som_training.png}
				\caption[Lernphase selbstorganisierender Karte]{Lernphase einer selbstorganisierenden Karte, Quelle: upload.wikimedia.org}
				\label{img:som_training}
			\end{center}
		\end{figure}
		
		Mit stetig wachsendem $t$ werden sowohl die Lernrate $\eta$ als auch der Adaptionsradius $\sigma$ ständig gesenkt, um Konvergenz zu erreichen. Im Falle der zeitabhängig definierten Parameter, wie sie in \cref{som_defs} beschreiben wurden, geschieht dies automatisch.
		
		Ist das Training nach $t_{\text{max}}$ beendet, so werden neue Reize $x_{\text{new}}$ mit dem selben Prinzip Ausgangsneuronen zugeordnet wie in der Trainingsphase: Die Reize werden dem Neuron $n^{t_{\text{max}}}_i$ auf der Karte zugeordnet, dessen Gewichtsvektoren $w^{t_{\text{max}}}_i$ den geringsten Abstand zum Reiz nach der Metrik $d_X$ haben. Auf diese Weise können neue Reize automatisch klassifiziert und visualisiert werden.
		\Cite{wiki-selbstorganisierende_karte}
		
	
	\subsection{Beispiel}
	\label{som_example}
	
		Als Beispiel für den Einsatz selbstorganisierender Karten wird in \cref{img:us_congress_votes} das Wahlverhalten des US-Kongresses untersucht. Als Eingaben dienten Tabellen mit Spalten zu spezifischen Wahlabgaben und entsprechenden Datensätzen zu jedem Mitglied des Kongresses. Das Netz hat die Mitglieder ihrem Wahlverhalten entsprechend so angeordnet, dass Mitglieder mit ähnlichem Wahlverhalten nah beieinander kartiert sind. Dies ist in den einzelnen Diagrammen zu erkennen, welche die konkreten Wahlentscheidungen der Mitglieder zu den verschiedenen Fragestellungen zeigen.
		\Cite{wiki-self_organizing_map}
		
		\begin{figure}[H]
			\begin{center}
				\includegraphics[width=1\linewidth]{images/us_congress_votes.png}
				\caption[Beispiel selbstorganisierender Karte]{Wahlverhalten des US-Kongresses auf hexagonaler Karte abgebildet, Quelle: en.wikipedia.org}
				\label{img:us_congress_votes}
			\end{center}
		\end{figure}		
		
%		Statistische Länderdaten der Weltbank aus dem Jahre 1992 werden in \cref{img:som_poverty_map} auf eine hexagonale Karte abgebildet. Die Daten bestehen je Land aus 39 Indikatoren. Zu erkennen ist, dass die Länder automatisch nach den 39 Kriterien auf der Karte angeordnet wurden und dabei eine deutliche Dimensionsreduktion stattfand. Die Farben wurden vom Algorithmus in einem zusätzlichen Schritt zugewiesen.
%		\Cite{poverty_worldmap}
%	
%		\begin{figure}[H]
%			\begin{center}
%				\includegraphics[width=1\linewidth]{images/povertymap_white.png}
%				\caption[Beispiel selbstorganisierender Karte]{Statistische Landesdaten auf hexagonaler Karte abgebildet, Quelle: www.cis.hut.fi}
%				\label{img:som_poverty_map}
%			\end{center}
%		\end{figure}
		
	\subsection{Bewertung}
	\label{som_bewertung}
	
		Selbstorganisierende Karten zeigen einen sehr einfachen Weg auf, hoch-dimensionale Daten zu visualisieren. Dabei sind die Ergebnisse von selbstorganisierenden Karten mit wenigen Neuronen ähnlich zu den Ergebnissen eines k-Means-Algorithmus\footnote{Ein \textbf{k-Means-Algorithmus} ist ein Verfahren zum Komprimieren oder Identifizieren von Daten, das auch zur Clusteranalyse verwendet wird}.
		\Cite{wiki-self_organizing_map}