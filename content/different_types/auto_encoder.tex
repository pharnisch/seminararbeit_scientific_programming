\section{Autoencoder}
\label{auto_encoder}

	Ein Autoencoder ist ein spezielles, unüberwachtes, vorwärtsgerichtetes künstliches neuronales Netz, dessen Einsatz es ist, die Dimension von Daten zu reduzieren\footnote{\textbf{Dimensionsreduktion} ist der Prozess vom Reduzieren der betrachteten Zufallsvariablen.}. Dies geschieht, indem Eingabevektoren der Länge $d$ innerhalb des Netzes in einen Zwischenvektor der Länge $p$ transformiert und anschließend auf einen Ausgabevektor der Länge $d$ (wie der Eingabevektor) abgebildet werden. Da laut Definition (siehe \cref{auto_encoder_defs}) gilt, dass $p \ll d$ und der $d$-elementige Ausgabevektor aus dem $p$-elementigen Zwischenvektor erzeugt wird, ist die Dimension (im Sinne der Mächtigkeit der minimalen Basis) der Ausgabe auf $p$ beschränkt. Unter anderem kann dies helfen, die (sonst zu hochdimensionalen) Daten zu visualisieren.
	
	Die Topologie eines Autoencoders (zu sehen in \cref{img:auto_encoder}) besteht aus mindestens drei Schichten, wobei die Eingabe- und die Ausgabeschicht gleiche Dimensionen haben. Weiterhin sind die Ein- und Ausgabevektoren gleich in ihrer Bedeutung. Die Neuronen der Schichten sind mit jedem Neuron der nächsten Schicht verbunden. Unter den versteckten Zwischenschichten muss es mindestens eine geben, dessen Neuronenanzahl, entsprechend der gewünschten Dimensionsreduktion, deutlich geringer ist, als die der Ein- und Ausgabeschicht. Durch den Flaschenhals der kleinsten versteckten Schicht (die mittlere Schicht in \cref{img:auto_encoder}) werden die Daten automatisch in ihrer Dimension reduziert und damit komprimiert. Dabei wird in \cref{img:auto_encoder} die Kodierung (extraction) mit $\Phi_{\text{extr}}$ und die Dekodierung (generation) mit $\Phi_{\text{gen}}$ bezeichnet, welche in dem Beispiel beide jeweils noch eine zusätzliche versteckte Zwischenschicht besitzen.
	
	Trotz des Einsatzes einer überwachten Lernstrategie wird bei Autoencodern von einem unüberwachten künstlichen neuronalen Netz gesprochen, da die Eingabedaten bereits den gewünschten Ausgabedaten entsprechen und somit keine zusätzlichen Informationen über den Sachverhalt erforderlich sind.
	\Cite{auto_encoder} \Cite{auto_encoder_und_convolutional_neural_networks}
	
	\begin{figure}[H]
		\begin{center}
			\includegraphics[width=0.6\linewidth]{images/auto_encoder.png}
			\caption[Autoencoder]{Autoencoder mit drei versteckten Schichten, Quelle: matthias-scholz.de}
			\label{img:auto_encoder}
		\end{center}
	\end{figure}
	
	\subsection{Definitionen}
	\label{auto_encoder_defs}

		Der Eingabe- und Ausgaberaum $X$ ist wie folgt definiert:
		\[ X \subseteq \mathbb{R}^d, d \in \mathbb{N} \text{ .} \]
		
		Die Menge der Testreize zum Trainieren des Netzes ist:
		\[ M \subseteq X, |M|=m \text{ .} \]
		
		Der Zwischenraum, dessen Element der Code am Flaschenhals des Netzes ist, ist folgender:
		\[ Z \subseteq \mathbb{R}^p, p \in \mathbb{N} \text{ .} \]
		Es gilt $p \ll d$, da die Dimension des Zwischenraumes erheblich geringer sein soll, um eine möglichst große Auswirkung zu erzielen.
		
		Das Tupel $(\Phi_{\text{extr}}, \Phi_{\text{gen}})$ repräsentiert die beiden Abbildungen, welche die Daten erst kodieren und daraufhin direkt wieder dekodieren sollen:
		\[ \Phi_{\text{extr}}: X \rightarrow Z \text{ ,} \]
		\[ \Phi_{\text{gen}}: Z \rightarrow \hat{X} \text{ .} \]
		Um zu verdeutlichen, dass Verluste auftreten können, bildet $\Phi_{\text{gen}}$ nicht auf $X$, sondern auf $\hat{X}$ ab. Im Idealfall gilt jedoch $\hat{X} = X$.
		
		Als Übertragungsfunktion $\Sigma$ wird die Skalarprodukt-Aktivierung verwendet. (siehe \cref{skalar_produkt_uebertragung})
		
		Die Aktivierungsfunktion $\varphi$ kann beliebig sein und funktioniert in diesem Kontext komponentenweise. Damit das angewendete Lernverfahren funktioniert, muss diese differenzierbar sein. Daher gehen wir in \cref{auto_encoder_ablauf} von der sigmoiden Aktivierungsfunktion mit $\beta = 1$ aus (siehe \cref{sigmoide_aktivierungs_funktion}).
		
		$W_i$ ist die Gewichtungsmatrix der $i$-ten Schicht. Dabei ist zu beachten, dass die Eingabeschicht nicht mitgezählt wird. Analog steht bei einem Gewicht $w_{ijk}$ der Index $i$ für die Schicht, $j$ für das Neuron der jeweiligen Schicht und $k$ für die Gewichtsposition innerhalb dieses Neurons.
		\Cite{auto_encoder} \Cite{auto_encoder_und_convolutional_neural_networks}
		
		Der quadratische Fehler $E$ (siehe L2-Fehler in \cref{L2_loss}) eines Netzdurchlaufs ist folgender:
		\[ E = \frac{1}{2} \sum_{k=1}^{d}{(x_{k} - \hat{x}_{k})^2} = \frac{1}{2} {\lVert x - \hat{x} \rVert}^{2} \text{ .} \]
		Dabei ist der hinzugefügte Faktor $\frac{1}{2}$ vereinfachend für das spätere Ableiten und ohne große Auswirkung auf das spätere Verfahren, da dort ohnehin als Faktor der freie Lernparameter $\eta$ vorkommt. Hier ist $k$ der Index der Eingabe- bzw. Ausgabevektorelemente.
		\Cite{backpropagation_example} \Cite{backpropagation_algorithm}
		
	\subsection{Netzdurchlauf}
	\label{auto_encoder_ablauf}
		
		Bei einem einfachen Autoencoder mit einer einzelnen versteckten Zwischenschicht werden die Gewichtsmatrizen $W_1$ und $W_2$ zufällig initialisiert. $W_1$ ist die Matrix der Gewichtungen der Zwischenschicht. $W_2$ ist die Matrix der Gewichtungen der Ausgabeschicht.
		
		Dann wird das Netz mit einer repräsentativen Teilmenge von $X$ trainiert.
		Zuerst wird durch die Zwischenschicht mit der Dimension $p$ der Code $z$ erzeugt:
		\[ z = \varphi(
		W_1
		\cdot
		x
		) 
		= \varphi(
		{\begin{pmatrix}
			w_{110} & w_{111} & \ldots & w_{11d}\\
			\vdots & \ddots & \ddots & \vdots\\
			w_{1p0} & w_{1p1} & \ldots & w_{1pd}\\
			\end{pmatrix}}
		\cdot
		\begin{pmatrix}
		1\\
		x_1\\
		\ldots\\
		x_d\\
		\end{pmatrix}
		)
		\text{ .}
		\]
		Hierbei sind in der Gewichtungsmatrix die on-Neuronen enthalten (wie in \cref{sec:kuenstliches_neuron} beschrieben). Dafür werden auch die Eingabereize mit dem fixen Wert $x_0 = 1$ ergänzt.
		
		Dann wird dieser Code $z$ von der Ausgabeschicht, welche wieder die selbe Dimension $d$ der Eingabeschicht hat, dekodiert:
		\[ \hat{x} = \varphi(
		W_2
		\cdot
		z
		) 
		= \varphi(
		{\begin{pmatrix}
			w_{210} & w_{211} & \ldots & w_{21p}\\
			\vdots & \ddots & \ddots & \vdots\\
			w_{2d0} & w_{2d1} & \ldots & w_{2dp}\\
			\end{pmatrix}}
		\cdot
		\begin{pmatrix}
		1\\
		z_1\\
		\ldots\\
		z_p\\
		\end{pmatrix}
		)
		\text{ .}
		\]
		
		Somit ergibt sich insgesamt der Vorgang:
		\[ \hat{x} = \varphi(
		W_2
		\cdot
		\varphi(
		W_1
		\cdot
		x
		)
		) \] 
		
		\[ = \varphi(
		{\begin{pmatrix}
			w_{210} & w_{211} & \ldots & w_{21p}\\
			\vdots & \ddots & \ddots & \vdots\\
			w_{2d0} & w_{2d1} & \ldots & w_{2dp}\\
			\end{pmatrix}}
		\cdot
		\varphi(
		{\begin{pmatrix}
			w_{110} & w_{111} & \ldots & w_{11d}\\
			\vdots & \ddots & \ddots & \vdots\\
			w_{1p0} & w_{1p1} & \ldots & w_{1pd}\\
			\end{pmatrix}}
		\cdot
		\begin{pmatrix}
		1\\
		x_1\\
		\ldots\\
		x_d\\
		\end{pmatrix}
		)
		)
		\text{ .}
		\]
		
		Um nach der Methode der kleinsten Quadrate\footnote{Die \textbf{Methode der kleinsten Quadrate} beschreibt das mathematische Standardverfahren der Ausgleichungsrechnung.} den Fehler $x - \hat{x}$ für jedes Element der  Testreiz-Menge $M$ zu minimieren, gibt es folgendes Minimierungsziel $J$:
		
		\begin{equation} \label{minimierungs_ziel}
		\begin{split}
		J(W_1, W_2) & = \sum_{l=1}^{m}{(x_l - (\Phi_{\text{gen}} \circ \Phi_{\text{extr}})(x_l))^2} \\
		& = \sum_{l=1}^{m}{(x_l - \varphi(W_2 \cdot \varphi(W_1 \cdot x_l)))^2} \\
		& \rightarrow min \text{ .}\\
		\end{split}
		\end{equation}
		\Cite{auto_encoder} \Cite{auto_encoder_und_convolutional_neural_networks}
	
	\subsection{Lernen mit Backpropagation}
	\label{auto_encoder_lernen}
	
		Das Problem aus \cref{minimierungs_ziel} soll durch das Training gelöst werden. Dazu wird hier eine Variante des Backpropagation-Lernens vorgestellt. Es sei angemerkt, dass das Lernen mit Backpropagation ineffizient wird, sobald es mehrere versteckte Schichten gibt. (Dabei kann ein Pretraining\footnote{Beim \textbf{Pretraining} werden die Gewichte des Netzes nicht zufällig initialisiert, sondern es werden Gewichtungen ermittelt und verwendet, die dem Zielergebnis bereits ungefähr entsprechen.} helfen.) 
		   
		Beim Training werden alle Elemente der Trainingsmenge $M$ durchlaufen. Jedes einzelne Element $x$ der Menge wird in das Netz eingegeben, um anschließend die jeweilige Fehlergröße $E = \frac{1}{2} {\lVert x - \hat{x} \rVert}^{2}$ zu bilden.
	
		Bei der Backpropagation für einen Lehr-Reiz wird zuerst für jedes Neuron im Netz berechnet, welchen Anteil es an dem gesamten Fehler $E$ hat. Für die Neuronen der Ausgabeschicht wird mithilfe der Kettenregel anschaulich berechnet, welche Auswirkung die Aktivierungsfunktion auf den totalen Fehler hatte und ebenfalls welche Auswirkung die Übertragungsfunktion auf die Aktivierungsfunktion hatte. Diese beiden Größen ergeben anschließend den Fehleranteil $\delta$:
		\begin{equation} \label{anteil_ausgabe_neuronen}
		\begin{split}
		\delta_2j & = \frac{\partial E}{\partial net_{2j}} = \frac{\partial E}{\partial \hat{x}_j} \cdot \frac{\partial \hat{x}_j}{\partial net_{2j}} \\
		& = \frac{\partial (\frac{1}{2}(x_1 - \hat{x}_1)^2 + \ldots + \frac{1}{2}(x_d - \hat{x}_d)^2)}{\partial \hat{x}_j} \cdot \frac{\partial \hat{x}_j}{\partial net_{2j}} \\
		& = \frac{\partial \frac{1}{2}(x_j - \hat{x}_j)^2}{\partial \hat{x}_j} \cdot \frac{\partial \hat{x}_j}{\partial net_{2j}} \\
		& = \frac{\partial \frac{1}{2}(x_j - \hat{x}_j)^2}{\partial \hat{x}_j} \cdot \frac{\partial \frac{1}{1 + e^{-net_{2j}}}}{\partial net_{2j}} \\
		& = (\hat{x}_j - x_j) \cdot (\hat{x}_j)(1 - \hat{x}_j) \text{ .}\\
		\end{split}
		\end{equation}
		Für die Neuronen der Zwischenschicht geschieht dies analog:
		\begin{equation} %\label{anteil_zwischen_neuronen}
		\begin{split}
		\delta_1j & = \frac{\partial E}{\partial net_{1j}} = \frac{\partial E}{\partial z_j} \cdot \frac{\partial z_j}{\partial net_{1j}} \\
		& = \frac{\partial (\frac{1}{2}(x_1 - \hat{x}_1)^2 + \ldots + \frac{1}{2}(x_d - \hat{x}_d)^2)}{\partial z_j} 
		\cdot \frac{\partial \frac{1}{1 + e^{-net_{1j}}}}{\partial net_{1j}} \\
		& = (\frac{\partial (\frac{1}{2}(x_1 - \hat{x}_1)^2 + \ldots + \frac{1}{2}(x_d - \hat{x}_d)^2)}{\partial z_j}) \cdot (z_j)(1 - z_j) \text{ .}\\
		\end{split}
		\end{equation}
	
\newpage
		Der Anteil der Zwischenschicht-Aktivierungsfunktion am totalen Fehler muss weiter aufgeschlüsselt werden, um ihn berechnen zu können, da die Ausgabe der Zwischenschicht alle Neuronen der Ausgabeschicht beeinflusst (und somit deren Übertragungs- und Aktivierungsfunktion):
		\begin{equation} \label{anteil_zwischen_neuronen}
		\begin{split}
		\delta_1j & = (\frac{\partial (\frac{1}{2}(x_1 - \hat{x}_1)^2 + \ldots + \frac{1}{2}(x_d - \hat{x}_d)^2)}{\partial z_j}) \cdot (z_j)(1 - z_j)\\
		& = (\sum_{l = 1}^{d}{ 
			\frac{\partial \frac{1}{2}(x_l - \hat{x}_l)^2}{\partial \hat{x}_l} 
			\cdot \frac{\partial \hat{x}_l}{\partial net_{2l}}
			\cdot \frac{\partial net_{2l}}{\partial z_j} 
		}) \cdot (z_j)(1 - z_j)\\
		& = (\sum_{l = 1}^{d}{ 
			\frac{\partial \frac{1}{2}(x_l - \hat{x}_l)^2}{\partial \hat{x}_l} 
			\cdot \frac{\partial \hat{x}_l}{\partial net_{2l}}
			\cdot \frac{\partial (w_{2l0}\cdot 1 +\ldots+ w_{2lp}\cdot z_p)}{\partial z_j} 
		}) \cdot (z_j)(1 - z_j)\\
		& = (\sum_{l = 1}^{d}{ 
			\frac{\partial \frac{1}{2}(x_l - \hat{x}_l)^2}{\partial \hat{x}_l} 
			\cdot \frac{\partial \hat{x}_l}{\partial net_{2l}}
			\cdot w_{2lj}
		}) \cdot (z_j)(1 - z_j)\\
		& \stackrel{\text{\cref{anteil_ausgabe_neuronen}}}{=} (\sum_{l = 1}^{d}{ 
			\delta_{2l}
			\cdot w_{2lj}
		}) \cdot (z_j)(1 - z_j) \text{ .}\\
		\end{split}
		\end{equation}
		
		Nachdem alle Fehler $\delta$ der Neuronen ermittelt wurden, können deren Gewichtsanpassungen mithilfe der Delta-Regel\footnote{Die \textbf{Delta-Regel} ist eine einfache Vorschrift zum Anpassen von Gewichten.} erfolgen, welche ergänzend zu \cref{anteil_ausgabe_neuronen} und \cref{anteil_zwischen_neuronen} ebenfalls die Lernrate $\eta$ und den Anteil der Gewichte an der Übertragungsfunktion mit einbezieht:
		\begin{equation} \label{anteil_gewicht_ausgabe_neuronen}
		\begin{split}
		\Delta_{2jk} & = \eta \cdot \delta_{2j} \cdot \frac{\partial net_{2j} }{\partial w_{2jk}} \\
		& = \eta \cdot \delta_{2j} \cdot \frac{\partial (w_{2j0}\cdot 1 +\ldots+ w_{2jp}\cdot z_p) }{\partial w_{2jk}} \\
		& = \eta \cdot \delta_{2j} \cdot z_k \text{ ,}\\
		\end{split}
		\end{equation}
		\begin{equation} \label{anteil_gewicht_zwischen_neuronen}
		\begin{split}
		\Delta_{1jk} & = \eta \cdot \delta_{1j} \cdot \frac{\partial net_{1j} }{\partial w_{1jk}} \\
		& = \eta \cdot \delta_{1j} \cdot \frac{\partial (w_{1j0}\cdot 1 +\ldots+ w_{1jd}\cdot x_d) }{\partial w_{1jk}} \\
		& = \eta \cdot \delta_{1j} \cdot x_k \text{ .}\\
		\end{split}
		\end{equation}
		
		Damit können die Gewichte modifiziert werden:
		\[ w_{ijk} \leftarrow w_{ijk} - \Delta_{ijk} \text{ .}\]
		(Das Symbol $\leftarrow$ entspricht in dieser Arbeit einer Variablenzuweisung. Je nach Literatur wird $\delta$ negiert definiert und dementsprechend $\Delta$ auf $w$ addiert.)
		
		Wenn dies für alle Trainigsreize ausgeführt wurde, ist das Training beendet. Während des Trainings kann der Lernparameter $\eta$ reduziert werden, um das Netz zu stabilisieren.
		\Cite{backpropagation_example} \Cite{backpropagation_algorithm}
	
	\subsection{Beispiel}
	\label{auto_encoder_example}
	
		Durch das zufällige Hinzufügen von Störungen in den Eingabedaten lernen Autoencoder, diese zu vernachlässigen. Wichtig ist, dass die Zieldaten zum Berechnen des Fehlers ohne Störungen bleiben. Es wird in diesem Fall von einem entrauschenden Autoencoder (englisch denoising autoencoder) gesprochen. In \cref{fig:handwritten_denoising_autoencoder} sind links die Originalfotos, mittig Fotos mit zufällig hinzugefügten Störungen und rechts die aus den gestörten Daten rekonstruierten Bilder zu sehen. Es ist zu erkennen, dass die Bilder der handgeschriebenen Zahlen trotz starker Störungen mit einem entrauschenden Autoencoder zufriedenstellend rekonstruiert werden konnten.
		\Cite{denoising_autoencoders}
		
		\begin{figure}[H]
			\begin{center}
				\includegraphics[width=1\linewidth]{images/denoising-example.png}
				\caption[Entrauschender Autoencoder]{Entrauschender Autoencoder, welcher störhafte Eingaben rekonstruiert, Quelle: towardsdatascience.com}
				\label{fig:handwritten_denoising_autoencoder}
			\end{center}
		\end{figure}
	
	\subsection{Bewertung}
	\label{autoencoder_bewertung}
	
		Autoencoder sind ein simples Modell künstlicher neuronaler Netze mit sehr typischen Strukturen. Der Einsatz von Autoencodern beginnt beim Visualisieren von hoch-dimensionalen Daten und reicht bis zur Bilderkennung (wobei mehrere Autoencoder hintereinander als Verbund fungieren). Dabei führen sowohl Autoencoder mit einer einzelnen versteckten Zwischenschicht und sigmoider Aktivierungsfunktion als auch jene mit ausschließlich linearen Aktivierungsfunktionen zu einem ähnlichen Ergebnis wie die Hauptkomponentenanalyse\footnote{Die \textbf{Hauptkomponentenanalyse} ist ein statistisches Verfahren zum Strukturieren umfangreicher Datensätze.}.
		\Cite{auto_encoder}