\chapter{Neurologischer Hintergrund}
\label{chap:neurologische_grundlagen}

\section{Neuronen}
\label{sec:neuron}

	Neuronen (auch als Nervenzellen bezeichnet) sind wichtige Zellen des Nervensystems von Tieren. Im Weiteren wird speziell das Nervensystem von Menschen betrachtet. Ein Neuron besteht aus einem Zellkörper, Dendriten und einem Neurit. Dabei werden im Zellkörper alle wichtigen Substanzen -- beispielsweise Proteine -- gebildet, die für die Funktion des Neurons benötigt werden. Die Dendriten eines Neurons dienen zum Empfangen von Erregungen anderer Neuronen. Das Neurit eines Neurons hingegen ist in erster Linie für die Weiterleitung dieser Impulse über seine Enden an ausgewählte Neuronen zuständig. Ein Neurit eines Neurons wird als Axon bezeichnet, wenn er von Gliazellen\footnote{\textbf{Gliazellen} sind zusammengefasst alle Zellen im Nervengewebe mit Ausnahme der Neuronen.} umhüllt ist. Der Ablauf der Übertragung wird in \cref{sec:ablauf_von_erregungsuebertragung} näher beschrieben.
	
	In \cref{fig:neuron} ist auf der linken Seite der Zellkörper, bezeichnet mit dem Synonym \glqq{}Soma\grqq{}, zu sehen. Links in der Darstellung sind die \glqq{}Dendrit[en]\grqq{} (in der Abbildung so bezeichnet) zu erkennen. Auf der rechten Seite sind die Enden eines Axons, welche auch Endknöpfchen genannt werden, zu sehen, bezeichnet mit \glqq{}Axonterminale\grqq{}.
	
	Nach Schätzungen besteht das menschliche Gehirn aus ungefähr $9 \cdot 10^{10}$ Neuronen und etwa ähnlich vielen Gliazellen.
	\Cite{wiki-nervenzelle}
	
	\begin{figure}[ht]
		\includegraphics[width=1\linewidth]{images/neuron.png}
		\caption[Neuron]{Neuron mit Beschriftung seiner Bestandteile, Quelle: de.wikipedia.org}
		\label{fig:neuron}
	\end{figure}
	
\section{Synapsen}
\label{sec:synapse}

	Eine Synapse ist die Schnittstelle zwischen dem Endknöpfchen eines Axons und der Membran\footnote{Eine \textbf{Membran} bezeichnet hier die dünne äußere Hülle.} eines Dendriten (seltener direkt der Membran des Zellkörpers) von zwei Neuronen. Eine Synapse ermöglicht es, Erregung in chemischer Form (seltener unmittelbar in elektrischer Form) weiterzugeben. Die Enden der Axone, die Endknöpfchen, werden auch Axonterminale genannt (siehe \cref{sec:neuron}). Für die Übertragung werden von den Endknöpfchen chemische Botenstoffe in den synaptischen Spalt\footnote{Der \textbf{synaptische Spalt} ist der Raum zwischen Axonterminalen und Dendritenenden} abgegeben und von den Rezeptoren auf der Membran der Dendritenenden aufgenommen. Auf den Dendriten des Empfängerneurons werden die empfangenen Erregungen zurück in elektrische Impulse gewandelt zu werden.
	
	Die Anzahl der Synapsen im Gehirn eines erwachsenen Menschen beträgt circa $10^{14}$. Bei einem einzelnen Neuron kann die Anzahl der Synapsen zwischen $1$ und $2 \cdot 10^{5}$ variieren.
	\Cite{wiki-synapse}
	
	Der Zustand der Synapse ist maßgeblich dafür verantwortlich, mit welcher Gewichtung Signale übertragen werden und ob sie sich hemmend oder erregend auswirken. \cite{informationsverarbeitung_im_menschlichen_gehirn} In \cref{sec:ablauf_von_erregungsuebertragung} wird genauer darauf eingegangen.
	
\section{Ablauf von Erregungsübertragung}
\label{sec:ablauf_von_erregungsuebertragung}
	
	Zu Beginn werden Reize von Rezeptoren (beispielsweise von Sinnesorganen) aufgenommen. Diese Reize gelangen durch das zentrale Nervensystem in das Gehirn. Sobald die Reize vom neuronalen Netz im Gehirn verarbeitet wurden, werden Effektoren (beispielsweise Muskeln) angesprochen und führen eine entsprechende Aktion aus. \cite{informationsverarbeitung_im_menschlichen_gehirn}
	
	Die Erregungen beliebig vieler Zellen werden von den Dendriten eines Neurons in Form chemischer Botenstoffe (sogenannter Neurotransmitter) aufgenommen. Abhängig davon, wie viele Rezeptoren sich auf der Dendritenmembran befinden und wie viele Botenstoffe im synaptischen Spalt sind (abgegeben von den vorherigen Endknöpfchen in der Kette), wird der elektrische Impuls unterschiedlich stark erzeugt. Anschließend breitet sich die elektrische Spannung über die Membran des Neurons aus. Je größer die Distanz ist, die überbrückt werden muss, desto mehr wird das Erregungssignal abgeschwächt. Im Zellkörper fließen die Erregungen der verschiedenen Dendriten in Form ihrer elektrischen Potentiale\footnote{Als \textbf{Potential} wird in diesem Kontext die elektrische Spannung auf der Neuronenmembran verstanden.} zusammen und werden summiert. Die Summation findet dabei sowohl räumlich als auch zeitlich statt:
	\begin{description}
		\item[Räumliche Summation:] Mehrere Synapsen erhalten zeitgleich Potentiale, welche sich im Zellkörper summieren.
		\item[Zeitliche Summation:] Eine Synapse erhält in genügend kurzen Abständen mehrere aufeinanderfolgende Potentiale, welche sich im Zellkörper summieren.
	\end{description}

	 Abhängig davon, ob die Summe der Erregungen in Form des elektrischen Potentials am Ort des Axonhügels\footnote{Ein \textbf{Axonhügel} ist der Ursprung eines Axons am Zellkörper eines Neurons.} das Schwellenpotential\footnote{Das \textbf{Schwellenpotential} ist die Potentialdifferenz die erforderlich ist, um ein Aktionspotential auszulösen.} überschreitet, wird das Aktionspotential\footnote{Das \textbf{Aktionspotential} ist eine temporäre Abweichung des Membranpotentials vom Ruhepotential.} ausgelöst. In diesem Falle würde das Aktionspotential dann über das Axon weitergeleitet und damit an den Axonterminalen auf andere Zellen übertragen werden.
	 \Cite{wiki-nervenzelle}
	 
	 Es gibt zudem eine Unterscheidung zwischen folgenden beiden Potentialen:
	 \begin{description}
	 	\item[Exzitatorisches Potential:] Stärkt die Bildung eines Aktionspotentials
	 	\item[Inhibitorisches Potential:] Hemmt die Bildung eines Aktionspotentials
	 \end{description}
 
 	Zwischen der Spitze des Aktionspotentials und der vollständigen Wiederherstellung des Ruhezustands vom Neuron vergeht die sogenannte Refraktärzeit. Innerhalb dieser Zeit sind die Natriumkanäle inaktiv und somit kann die Membran keine Erregungen in Form von elektronischer Spannung aufnehmen.
 	\Cite{refraktaerzeit}

\section{Neuronale Netze}
\label{sec:neuronales_netz}

	Als neuronales Netz wird ein Verbund von Neuronen verstanden, die einen Teil eines Nervensystems mit einer bestimmten Funktion darstellen. Die Neuronen innerhalb eines neuronalen Netzes sind via Synapsen miteinander verbunden und ermöglichen so die Weitergabe von Erregungen über viele Neuronen hinweg. Dabei kann das Signal von einem Neuron an mehrere andere Neuronen übermittelt werden (Divergenz) oder auch von einem Neuron mehrere Signale aufgenommen werden (Konvergenz).
	
	Werden Signale in hoher Frequenz übertragen, kann dies dazu führen, dass die Übertragung langfristig stärker gewichtet wird (Langzeit-Potenzierung). Auf der anderen Seite kann eine niedrige Frequenz zu einer Abschwächung der Gewichtung führen (Langzeit-Depression). Durch diese Phänomene und im Weiteren des Phänomens der neuronalen Plastizität (siehe \cref{sec:neuronale_plastizitaet}) wird es einem neuronalen Netz beispielsweise ermöglicht, komplexe Muster zu erlernen. 
	\Cite{wiki-neuronales_netz}

\section{Neuronale Plastizität}
\label{sec:neuronale_plastizitaet}

	Neuronale Plastizität beschreibt die regenerative Fähigkeit eines neuronalen Netzes: Wenn Zellen im Gehirn absterben, können zum Ausgleich neue Zellen ihre Position einnehmen, die zusammen mit neuen Synapsenverbindungen einem Verfall des Netzes entgegenwirken.
	\Cite{neuronale_plastizitaet_gehirn_und_wirklichkeit_veraendern}

	Neuronale Plastizität beschreibt ebenfalls das Phänomen, dass Synapsen, Neuronen oder ganze Regionen im Gehirn sich in ihrer Anatomie und Funktion verändern können, um bestehende Prozesse zu optimieren. Es können beispielsweise Synapsen zwischen Neuronen entstehen oder wegfallen. Auch können Gewichtungen von Eingangssignalen eines jeden Neurons kurz- oder langfristig verändert werden. 
	
	Die neuronale Plastizität insgesamt ist ausschlaggebend für das Lernen sowie das Bilden eines Gedächtnisses und beschreibt eine gewisse Formbarkeit und Dynamik des Netzes.
	\Cite{wiki-neuronale_plastizitaet}
	
\section{Lernvorgang}
\label{sec:Lernvorgang}

	Zum Zeitpunkt der Geburt eines Menschen existieren bei diesem bereits sämtliche Neuronen im Gehirn. Es fehlen zu Beginn jedoch die Synapsen (also die Verbindungen zwischen den Neuronen). Lernen wird dadurch ermöglicht, dass das neuronale Netz im Gehirn formbar ist (siehe \cref{sec:neuronale_plastizitaet}).
	
	Nach simplen Theorien bilden sich die Snypasen vor allem durch Training des Menschen aus. Je häufiger das Training durchgeführt wird, desto stärker respektive besser (eventuell durch andere Gewichtungen) werden diese Verbindungen. Nach dieser Theorie vergisst der Mensch, wenn er diese Verbindungen nicht mehr nutzt, also durch das Auslassen von Training, wobei Neuronen absterben. Nach der These des Psychologen Donald Hebb wird dies bestätigt: Die häufige Nutzung \glqq{}vor- und nachgelagerte[r] Neuronen\grqq{} stärkt die \glqq{}Wirkung der Synapse, und umgekehrt.\grqq{}.
	\Cite{informationsverarbeitung_im_menschlichen_gehirn}
	
	\subsection{Kurzzeitgedächtnis}
	
		Wenn Impulse beginnen, in Kreisen von Neuronen durch das Gehirn geleitet zu werden, ist die Information im Stadium des Kurzgedächtnisses und kann nach einiger Zeit wieder verloren gehen. Die Spuren haben sich also noch nicht verfestigt.
		\cite{informationsverarbeitung_im_menschlichen_gehirn}
	
	\subsection{Langzeitgedächtnis}
	
		Bei Verbindungen zwischen Neuronen, die sich durch Langzeit-Potenzierung (siehe \cref{sec:neuronales_netz}) festigen, entstehen Engramme\footnote{Als \textbf{Engramm} wird eine Spur von Neuronen bezeichnet, welche durch eine starke Gewichtung der synaptischen Schnittstellen verfestigt ist. \cite{engramm}}. Diese sind verantwortlich für das Langzeitgedächtnis.
		\cite{informationsverarbeitung_im_menschlichen_gehirn}
	
	