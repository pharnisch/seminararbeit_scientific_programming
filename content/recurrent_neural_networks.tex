\section{Rekurrente neuronale Netze}
\label{rnn}

	Rekurrente neuronale Netze sind Netze, dessen Neuronen nicht nur Verbindungen zu Neuronen in den Schichten vor ihnen haben, sondern auch zur eigenen oder weiter hinterliegenden Schichten. Sie helfen ein Lernverhalten für Eingabesequenzen zu erzeugen, bei dem das Netz sich merken soll, welche Eingabereize vorher stattfanden. Dieses Wissen vom Vorgeschehen fließt dann ein in die Berechnung der jeweiligen Ausgabe. (beispielsweise bei Marktprognosen oder Texterzeugung -- im Grunde genommen überall, wo stochastisch gesehen bedingte Wahrscheinlichkeiten auftauchen) Enorm abheben tun sich die rekurrenten neuronalen Netze von gewöhnlichen vorwärtsgerichteten neuronalen Netzen dadurch, dass sie nicht nur auf eine Eingabe und eine Ausgabe, die zusammengehören, beschränkt sind. Es können weiterreichende Probleme mit zusammenhängenden Folgen von Ein- und Ausgaben gelöst werden. 
	
	In \cref{fig:rnn_relations} sind verschiedene Formen von rekurrenten neuronalen Netzen zu sehen, dessen Iterationsschritte aufgeschlüsselt von links nach rechts visualisiert wurden. Dabei sind die Eingabevektoren als rote Kästen und die Ausgabevektoren als blaue Kästen gezeichnet. Die grüne rekurrente Zwischenschicht beeinflusst jeweils die Zwischenschicht des nächsten Folgegliedes, also sich selbst im darauffolgenden Zeitschritt. Es ist zu erkennen, welche Kombinationen von Ein- und Ausgabesequenzen sich durch rekurrente neuronale Netze ergeben:
	\begin{description}
		\item[Ein Eingabevektor, ein Ausgabevektor:] In der Darstellung ist unter \glqq{}one to one\grqq{} zu erkennen, auf welche Architektur vorwärtsgerichtete neuronale Netze beschränkt sind. Eine mögliche Anwendungssituation dafür ist das Approximieren einer Funktion. Dabei sind die verschiedenen Durchgänge immer unabhängig voneinander.
		\item[Ein Eingabevektor, mehrere Ausgabevektoren:] Die als \glqq{}one to many\grqq{} gekennzeichnete Architektur wird erst durch Rückkopplungen möglich. Beispielsweise ist ein Anwendungsszenario das Erstellen von Bildunterschriften. (Ein Bild stellt die Eingabe dar und die Ausgabe ist eine Folge von Wörtern.)
		\item[Mehrere Eingabevektoren, ein Ausgabevektor:] Unter \glqq{}many to one\grqq{} ist die umgekehrte Situation von \glqq{}one to many\grqq{} zu sehen, die ebenfalls erst durch rekurrente Netze ermöglicht wird. Ein Beispiel hierfür ist die Validierung dafür, ob ein Satz (als eine Folge von Wörtern oder Zeichen) grammatikalisch fehlerfrei ist.
		\item[Mehrere Eingabevektoren, mehrere Ausgabevektoren:] Zuletzt werden in der Abbildung zwei Architekturen \glqq{}many to many\grqq{} gezeigt, die gemein haben, mehrere Eingabevektoren zu mehreren Ausgabevektoren verarbeiten zu können. Diese durch rekurrente neuronale Netze ermöglichten Konstruktion lassen sich untergliedern in eine synchrone oder nicht-synchrone Überlagerung von Ein- und Ausgabevektoren. Synchron muss eine Architektur sein für die Erzeugung einer Folge von Bildunterschriften passend zu einer Folge von Bildern in einem Film. Keine Synchronität wird dagegen erwartet bei der Übersetzung von Texten in eine andere Sprache hinein.
	\end{description}
	
	\begin{figure}[htb]
		\includegraphics[width=1\linewidth]{images/rnn_relations.png}
		\caption[Rekurrente Netz-Architekturen]{Auswahl verschiedener Netz-Architekturen rekurrenter neuronaler Netze, Quelle: karpathy.github.io}
		\label{fig:rnn_relations}
	\end{figure}
	
\newpage
	Dabei kann zwischen verschiedenen Rückkopplungen unterschieden werden:
	\begin{description}
		\item[Direkte Rückkopplung:] Ein Neuron verwendet seine eigene Ausgabe als zusätzliche Eingabe.
		\item[Indirekte Rückkopplung:] Ein Neuron leitet seine Ausgabe an ein Neuron der vorherigen Schicht.
		\item[Seitliche Rückkopplung:] Ein Neuron leitet seine Ausgabe an ein anderes Neuron derselben Schicht weiter.
	\end{description}
	In diesem Unterkapitel wird ein rekurrentes neuronales Netz mit einer versteckten Schicht vorgestellt, welches direkt rückgekoppelt ist.
	\Cite{rnn_introduction}
	
	\subsection{Definitionen}
	\label{rnn_defs}
	
		Die Zeitschritte innerhalb des rekursiven Netzdurchlaufes werden mit dem Index $t$ gekennzeichnet:
		\[ t \in \{0, 1, \,\dots\, m\} \text{ .} \]
		
		Eingaberaum $X$, Zwischenraum $Z$ und Ausgaberaum $Y$ sind wie folgt definiert:
		\[ X \subseteq \mathbb{R}^n, Z \subseteq \mathbb{R}^p, Y \subseteq \mathbb{R}^d, n, p, d \in \mathbb{N} \text{ .} \]
		
		Die Trainingsdaten einer Folge von zusammenhängenden Eingabevektoren $x \in X$ sind in einer Menge $M$ zusammengefasst:
		\[ M = \{x \mid x \in X \}, |M| = m \in \mathbb{N}  \text{ .} \]
		Eine Trainingsphase kann dabei beliebig viele Folgen $M$ enthalten. Im folgenden wird ohne Beschränkung der Allgemeinheit jedoch vereinfacht von lediglich einer Trainingsreihe $M$ ausgegangen.

		Die verschiedenen Gewichtsmatrizen sind wie folgt definiert: $U$ ist die Gewichtsmatrix der Zwischenschicht in Bezug auf den Eingabevektor $x^t$, $W$ ist die Gewichtsmatrix der Zwischenschicht in Bezug auf die Ausgabe der Zwischenschicht im letzten Zeitschritt $s^{t-1}$ und $V$ ist die Gewichtsmatrix der Ausgabeschicht in Bezug auf die Ausgabe der Zwischenschicht im aktuellen Zeitschritt $s^t$:
		\[ U \in \mathbb{R}^{n \times p}, W \in \mathbb{R}^{p \times p}, V \in \mathbb{R}^{p \times d} \text{ .} \]
		Wichtig für das Verständnis ist, dass innerhalb einer Sequenz für jeden Zeitschritt die Gewichtungen gleich bleiben, sie sind also unabhängig von $t$.
		
		Ferner sei im Zeitschritt $t$ die Variable $net^t_{sj}$ das Ergebnis der Übertragungsfunktion des $j$-ten Zwischenschichtneurons und $net^t_{oj}$ das Ergebnis der Übertragungsfunktion des $j$-ten Ausgabeneurons.
		
		Der totale Fehler $E$ einer ganzen Folge von Eingaben wird berechnet als:
		\begin{equation}\label{rnn_error}
		E = \sum_{t=1}^{m} E^t \text{ ,}
		\end{equation}
		wobei $E^t$ der Fehler der Ausgabe des jeweiligen Zeitschrittes ist.
		\Cite{rnn_tutorial_introduction}
		
	\subsection{Netzdurchlauf}
	\label{subsec:rnn_netzdurchlauf}
	
		Die Gewichtsmatrizen werden zu Beginn zufällig initialisiert. Die Ausgaben der rekurrenten Zwischenschichtneuronen berechnet sich ähnlich wie bei vorwärtsgerichteten Netzen. Der einzige Unterschied ist, dass zusätzlich zu dem Eingabevektor als weitere Eingabe die Ausgabe der Zwischenschicht aus dem vorherigen Zeitschritt (ebenfalls gewichtet) einfließt:
		\[ s_t = \varphi(W\cdot x_t + U\cdot s_{t-1}) \text{ .} \]
		Dabei bestimmt die Gewichtsmatrix $U$ den Einfluss des \glqq{}Gedächtnisses\grqq{} in Form der Zwischenschichtausgaben des vorherigen Zeitschrittes. Da $s_{t}$ rekursiv von allen anderen Vorgängern abhängig ist, wird ein Langzeitgedächtnis des Netzes ermöglicht.
		
		Der Einfluss der Zwischenschicht aus dem letzten Zeitschritt wird in Diagrammen oftmals mit einem Pfeil der Neuronen auf sich selbst dargestellt. Eine Alternative Darstellung ist möglich durch das Aufschlüsseln der Rekursion in ihren iterativen Ablauf (zu sehen in \cref{fig:rnn_unfold}).
		
		\begin{figure}[htb]
			\includegraphics[width=1\linewidth]{images/rnn_unfold.png}
			\caption[Ausgeklapptes rekurrentes neuronales Netz]{Rekurrentes neuronales Netz, welches zur Veranschaulichung ausgeklappt wurde, Quelle: www.wildml.com}
			\label{fig:rnn_unfold}
		\end{figure}
		
		Im ersten Schritt liegen noch keine ehemaligen Ausgaben der Zwischenschicht vor. Anschaulich kann das erste Glied der Folge nicht durch vorherige beeinflusst werden, da diese nicht existieren. Daher ist $s^0$ wie folgt definiert:
		\[ s^{0} = 
		\begin{pmatrix}
		s^0_{1}=0\\
		\vdots\\
		s^0_p=0\\
		\end{pmatrix}
		\text{ .}
		\]
		
		Danach kann die Ausgabe $o^t$ berechnet werden:
		\[ o^t = \varphi(V \cdot s^t) \text{ .} \]
		An welchen Zeitschritten die Ausgaben $o^t$ relevant sind, hängt ganz davon ab, welcher Sachverhalt modelliert wird. Dementsprechend kann es möglich sein, dass beispielsweise nur der Wert des letzten Zeitschrittes ausschlaggebend ist.
		\Cite{rnn_tutorial_introduction}

	\subsection{Lernen mit Backpropagation}
	
		Da das Lernen rekurrenter neuronaler Netze anschaulich mehrere Zeitschritte der Schrittfolgen rückwärts durchschreiten muss, wird in englischer Literatur auch häufig der Begriff \glqq{}Backpropagation through time\grqq{} verwendet, wobei mit Zeit anschaulich der Schritt-Index $t$ gemeint ist. Grundsätzlich bleibt in diesem Fall das Verfahren jedoch gleich - es wird durch die Rekursion lediglich aufwendiger nach den jeweiligen Anteilen am Gesamtfehler $E$ aufzuschlüsseln.
		
		Es wird nun davon ausgegangen, dass die Ausgaben $o^t$ jeden Zeitschrittes $t$ relevant sind für das Modell, weswegen auch in der Definition \cref{rnn_error} für $E$ alle Teilfehler $E^t$ aufsummiert werden. Zum besseren Verständnis sei hier erneut angemerkt, dass lediglich von einer Trainingsdatenreihe $M$ ausgegangen wird - es können natürlich auch mehrere Trainingsreihen $M_1, M_2, \ldots$ Teil der Trainingsphase sein. Eine Trainingsreihe entspricht dabei beispielsweise einem Satz, welcher mehrere Wörter $x$ beinhaltet.
		
		Nach dem Netzdurchlauf der Trainingsdaten aus $M$ (siehe \cref{subsec:rnn_netzdurchlauf}) kann der totale Fehler $E$ gebildet werden. Damit sinnvolle Modifikationen an den Gewichtungen vorgenommen werden können, muss zuerst herausgefunden werden, welchen Anteil die einzelnen Gewichte am gesamten Fehler haben. 
	\newpage % war vorher ein Absatz
		Anschaulich wird in \cref{v_basic_formula} der Fehleranteil von $v_{jk}$ berechnet. Es ist bekannt, dass $v_{jk}$ nur $net^t_{oj}$ beeinflusst, $net^t_{oj}$ nur $o^t_j$ beeinflusst und die Größe $o^t_j$ nur den Fehleranteil $E^t$ beeinflusst. Insgesamt beeinflussen zudem alle Teilfehler $E^t$ den insgesamten Fehler $E$:
		\begin{equation}\label{v_basic_formula}
		\begin{split}
		\frac{\partial E}{\partial v_{jk}} & =
		\sum_{t=1}^{m} \frac{\partial E}{\partial E^t} \frac{\partial E^t}{\partial o^t_j} \frac{\partial o^t_j}{\partial net^t_{oj}} \frac{\partial net^t_{oj}}{\partial v_{jk}}\\
		& \stackrel{\frac{\partial E}{\partial E^t} = 1}{=} \sum_{t=1}^{m} \frac{\partial E^t}{\partial o^t_j} \frac{\partial o^t_j}{\partial net^t_{oj}} \frac{\partial net^t_{oj}}{\partial v_{jk}} \text{ .}\\
		\end{split}
		\end{equation}
		Dabei steht der Index $j$ für das Neuron der jeweiligen Schicht und $k$ für das Gewicht des jeweiligen Neurons.
		
		In \Cite{rnn_tutorial_bptt_and_vanishing_gradients} wird eine Notation verwendet, in der die Vektoren und Matrizen als Ganzes eingesetzt werden. Dies ist mathematisch nicht sauber, jedoch vermeidet es im späteren Kontext viele verschachtelte Summationen. Diese Notation ist zu sehen in \cref{V_basic_formula}:
		\begin{equation}\label{V_basic_formula}
		\begin{split}
		\frac{\partial E}{\partial V} & =
		\sum_{t=1}^{m} \frac{\partial E}{\partial E^t} \frac{\partial E^t}{\partial o^t} \frac{\partial o^t}{\partial net^t_{o}} \frac{\partial net^t_{o}}{\partial V}\\
		& \stackrel{\frac{\partial E}{\partial E^t} = 1}{=} \sum_{t=1}^{m} \frac{\partial E^t}{\partial o^t} \frac{\partial o^t}{\partial net^t_{o}} \frac{\partial net^t_{o}}{\partial V} \text{ .}\\
		\end{split}
		\end{equation}
		
		Bei der Herleitung des Gradienten für die Gewichte $u$ und $w$ werden pro Teilfehler $E^t$ alle bis dahin durchlaufenen Zeitpunkte $1, \ldots, t$ miteinbezogen. Dadurch steigt die Komplexität sehr und um ein Verständnis beim Leser zu unterstützen, wird für diese Parameter lediglich die Notation aus \Cite{rnn_tutorial_bptt_and_vanishing_gradients} verwendet:
		\[ \frac{\partial E}{\partial U}  =
		\sum_{t=1}^{m} \frac{\partial E}{\partial E^t} \frac{\partial E^t}{\partial o^t} \frac{\partial o^t}{\partial s^t} \frac{\partial s^t}{\partial U}
		\stackrel{\frac{\partial E}{\partial E^t} = 1}{=} \sum_{t=1}^{m} \frac{\partial E^t}{\partial o^t} \frac{\partial o^t}{\partial s^t} \frac{\partial s^t}{\partial U} \text{ .}\] 
		
		Um jeden Zeitschritt abzubilden, wird folgendermaßen summiert:
		\[ \frac{\partial E}{\partial U} = \sum_{t=1}^{m} \frac{\partial E^t}{\partial o^t} \frac{\partial o^t}{\partial s^t} \sum_{k=1}^{t} \frac{\partial s^t}{\partial s^k} \frac{\partial s^k}{\partial U} \text{ .} \] 
		
		Jeder Zeitschritt hängt rekursiv von allen vorherigen ab (in \cref{fig:rnn_example_error_part} am Beispiel eines Fehlers $E^3$ zu erkennen):
		\[ \frac{\partial E}{\partial U} = \sum_{t=1}^{m} \frac{\partial E^t}{\partial o^t}  \frac{\partial o^t}{\partial s^t} \sum_{k=1}^{t} \left( \prod_{l=k+1}^{t} \frac{\partial s^l}{\partial s^{l-1}} \right) \frac{\partial s^k}{\partial U} \text{ .} \] 
		
		\begin{figure}[htb]
			\includegraphics[width=1\linewidth]{images/rnn-bptt-with-gradients.png}
			\caption[Rekursiver Fehleranteil]{Visualisierung der Backpropagation am Beispiel des Fehlers $E^3$, Quelle: www.wildml.com}
			\label{fig:rnn_example_error_part}
		\end{figure}
		
		Insgesamt ergibt dies beim weiteren Aufschlüsseln nach der involvierten Übertragungsfunktion $\Sigma$:
		\begin{equation}\label{U_basic_formula}
		\begin{split}
		\frac{\partial E}{\partial U} & = \sum_{t=1}^{m} \frac{\partial E^t}{\partial o^t} \frac{\partial o^t}{\partial net^t_{o}} \frac{\partial net^t_o}{\partial s^t} \sum_{k=1}^{t} \left( \prod_{l=k+1}^{t} \frac{\partial s^l}{\partial net^l_s} \frac{\partial net^l_s}{\partial s^{l-1}} \right) \frac{\partial s^k}{\partial net^k_s} \frac{\partial net^k_s}{\partial U} \text{ .}\\
		\end{split}
		\end{equation}
		
		Analog zu $u$ lässt sich der Fehleranteil von $w$ beschreiben:
		\begin{equation}\label{W_basic_formula}
		\begin{split}
		\frac{\partial E}{\partial W} & = \sum_{t=1}^{m} \frac{\partial E^t}{\partial o^t} \frac{\partial o^t}{\partial net^t_{o}} \frac{\partial net^t_o}{\partial s^t} \sum_{k=1}^{t} \left( \prod_{l=k+1}^{t} \frac{\partial s^l}{\partial net^l_s} \frac{\partial net^l_s}{\partial s^{l-1}} \right) \frac{\partial s^k}{\partial net^k_s} \frac{\partial net^k_s}{\partial W} \text{ .}\\
		\end{split}
		\end{equation}
		
\newpage % wegen Formel-Hurenkindern		
		
		Die Berechnung der verschiedenen Ableitungen ist ab diesem Punkt nicht notwendig für das Verständnis. Die Ergebnisse hängen dabei von den gewählten Fehler-, Aktivierungs- und Übertragungsfunktionen ab. Die Gewichte werden anschließend modifiziert:
		\[ v_{jk} \leftarrow v_{jk} - \eta \cdot \frac{\partial E}{\partial v_{jk}} \text{ ,}\]
		\[ u_{jk} \leftarrow u_{jk} - \eta \cdot \frac{\partial E}{\partial u_{jk}} \text{ ,}\]
		\[ w_{jk} \leftarrow w_{jk} - \eta \cdot \frac{\partial E}{\partial w_{jk}} \text{ .}\]
		\Cite{rnn_tutorial_bptt_and_vanishing_gradients} \Cite{rnn_guide}
	
	\subsection{Beispiel}
	\label{rnn_example}
		
		Ein mögliches Einsatzgebiet von rekurrenten neuronalen Netzen ist das Vorschlagen von Programmcode in Entwicklungsumgebungen. Dafür kann ein Befehl oder ein größerer Bereich des Quellcodes als Sequenz von aufeinanderfolgenden Buchstaben aufgegriffen werden. Beim Training solch eines Netzes mit dem gesamten Linux-Quellcode (474 MB) wurde ersichtlich, dass die korrekte Syntax der Programmiersprache C vom Netz verinnerlicht wurde (siehe \cref{fig:rnn_code}). Zudem wurden gängige Praktiken, wie Einrückungen, korrekt beachtet. Es sei angemerkt, dass bei diesem einfachen Netz komplexere Logik nicht abgebildet werden konnte, es könnte also in diesem simplen Modell nur unterstützend dienen.
		\Cite{andrej_karpathy}
		
\begin{figure}[H]%
{\tiny
\begin{lstlisting}[frame=trBL, breaklines=true, label={lst:rnn_code}]
/*
* Increment the size file of the new incorrect UI_FILTER group information
* of the size generatively.
*/
static int indicate_policy(void)
{
  int error;
  if (fd == MARN_EPT) {
    /*
    * The kernel blank will coeld it to userspace.
    */
    if (ss->segment < mem_total)
      unblock_graph_and_set_blocked();
    else
      ret = 1;
    goto bail;
  }
  segaddr = in_SB(in.addr);
  selector = seg / 16;
  setup_works = true;
  for (i = 0; i < blocks; i++) {
    seq = buf[i++];
    bpf = bd->bd.next + i * search;
    if (fd) {
      current = blocked;
    }
  }
  rw->name = "Getjbbregs";
  bprm_self_clearl(&iv->version);
  regs->new = blocks[(BPF_STATS << info->historidac)] | PFMR_CLOBATHINC_SECONDS << 12;
  return segtable;
}
\end{lstlisting}
}
\caption[Von rekurrentem neuronalen Netz generierter C-Code]{Von rekurrentem neuronalen Netz generierter C-Code, Quelle: karpathy.github.io}
\label{fig:rnn_code}
\end{figure}
	
	\subsection{Bewertung}
	
		Wenn Beziehungen zwischen Ein- oder Ausgabefolgen modelliert werden sollen, muss von den vorwärtsgerichteten neuronalen Netzen abgewichen und sich den rekurrenten neuronalen Netzen gewidmet werden. Rekurrente neuronale Netze werden typischerweise eingesetzt, um Vorhersagen für Zeitreihen zu treffen respektive bestimmte Sequenzen wie Wortfolgen, Musiktöne oder Bilderreihen zu erzeugen.
		
		Bei sehr langen Rekursionsketten wird der Rechenaufwand für die Backpropagation immer teurer. Zudem entstehen weitere Probleme, wie das Verschwinden des Gradienten\footnote{Der \textbf{Gradient} beschreibt allgemein die Steigung von mehrdimensionalen Funktionen. In dem hier verwendeten Kontext beschreibt er speziell den Anteil von Gewichten am Gesamtfehler $E$.} (englisch vanishing gradient) oder das exponentielle Wachstum von Gradienten (englisch exploding gradient).
		Anstatt erst am Ende mit Backpropagation zu trainieren, kann auch jeweils für eine bestimmte Anzahl von Schritten den Lernvorgang vollzogen werden. (englisch truncated backpropagation through time) Eine gängige Größe ist dabei $100$.
		
		Durch die Multiplikation vieler Faktoren bei der Backpropagation (auch durch Einsatz mehrerer Schichten, aber vielmehr durch die rekursive Nutzung einer Schicht) neigen die Gradienten dazu, gegen 0 zu konvergieren oder exponentiell zu wachsen. Das Problem der zu schnell wachsenden Gradienten kann einfach gelöst werden:
		\begin{itemize}
			\item Durchführung von Backpropgation in mehreren Etappen für begrenzte Anzahl an Rekursionsschritten
			\item Herstellen einer Grenze für die Gradientennorm
		\end{itemize}
		Da auf die Begrenzung der Schritte in diesem Abschnitt bereits näher eingegangen wurde, folgt nun die Funktionsweise der Gradientennormgrenze. Es wird in diesem Verfahren während der Backpropagation ständig geprüft, ob die $\Xi$-Norm des Gradienten einen bestimmten Wert $\xi$ übersteigt. In solch einem Fall würde der berechnete Gradient $\frac{\partial E}{\partial G}$ (wobei $G$ eine der Gewichtsmatrizen sei) während seines Berechnungsprozesses immer wieder modifiziert werden:
		\[ \frac{\partial E}{\partial G} \leftarrow  \frac{\partial E}{\partial G} \cdot \frac{ \xi }{ \lVert \frac{\partial E}{\partial G} \rVert_{\Xi} } \text{ .} \]
		Damit wird der Gradient auf die Länge $\xi$ \glqq{}gekürzt\grqq{}. Gängige Werte dafür sind $\Xi=1$ und $\xi=0.5$.
		
		Für das Problem des verschwindenden Gradienten gibt es jedoch nicht solch einfache Lösungen, da bei sehr kleinen Zahlen die Computer-Zahlendarstellung zu schnell Ungenauigkeiten verursacht. Jedoch gibt es andere Architekturen, die mit dieser Problematik umgehen können. Besonders vielversprechend ist eine Architektur, welche sich LSTM\footnote{\textbf{LSTM} (long short-term memory) ist der Name einer rekurrenten Netzarchitektur, die Antwort auf das Problem des verschwindenden Gradienten sein soll.} nennt.